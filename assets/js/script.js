// // Navbar Fixed
// window.onscroll = function () {
//   const header = document.querySelector("header");
//   const bottomNav = document.querySelector("#bottom-navigation");
//   const fixedNav = header.offsetTop;
//   const toTop = document.querySelector("#to-top");
//
//   if (window.pageYOffset > fixedNav) {
//     header.classList.add("navbar-fixed");
//     bottomNav.classList.add("navbar-bottom");
//     toTop.classList.remove("hidden");
//     toTop.classList.add("flex");
//   } else {
//     header.classList.remove("navbar-fixed");
//     bottomNav.classList.remove("navbar-bottom");
//     toTop.classList.remove("flex");
//     toTop.classList.add("hidden");
//   }
// };
//
// // Dark Mode
// const darkToggle = document.querySelector("#dark-toggle");
// const html = document.querySelector("html");
//
// if (darkToggle) {
//   darkToggle.addEventListener("click", function () {
//     if (darkToggle.checked) {
//       html.classList.add("dark");
//       localStorage.theme = "dark";
//     } else {
//       html.classList.remove("dark");
//       localStorage.theme = "light";
//     }
//   });
//
//   // TOGGLE BUTTON DARK MODE (LOCAL STORAGE) BASED ON USER PREFERENCE
//   // On page load or when changing themes, best to add inline in `head` to avoid FOUC
//   darkToggle.checked = localStorage.theme === "dark" || (!("theme" in localStorage) && window.matchMedia("(prefers-color-scheme: dark)").matches);
// }

const accordionHeader = document.querySelectorAll('.accordion-header')

accordionHeader.forEach(accordionHeader => {
    accordionHeader.addEventListener("click", event => {
        accordionHeader.classList.toggle("active")
        const accordionBody = accordionHeader.nextElementSibling
        if (accordionHeader.classList.contains("active")) {
            accordionBody.style.maxHeight = accordionBody.scrollHeight + "px"
        } else {
            accordionBody.style.maxHeight = 0
        }
    })
})
