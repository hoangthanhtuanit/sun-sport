// play & pause
const playPauseBtn = document.querySelector(".play-pause-btn");
const playBtn = document.querySelector(".paly-btn");
const pauseBtn = document.querySelector(".pause-btn");
// video & container
const video = document.querySelector(".video");
const container = document.querySelector(".container-video");
// timeline
const thumb = document.querySelector(".thumb");
const durationTime = document.querySelector(".duration-time");
const currentTime = document.querySelector(".current-time");
// speed time
const forwardTime = document.querySelector(".forward-time");
const backTime = document.querySelector(".backward-time");
// forward & backward time
const forwardBtn = document.querySelector(".forward-btn");
const backwardBtn = document.querySelector(".backward-btn");
// mode view
const fullScreen = document.querySelector(".full-screen");
const fullScreenExit = document.querySelector(".full-screen-exit");
const miniScreen = document.querySelector(".miniScreen");
const fullScreenOpen = document.querySelector(".full-screen-open");
// volume
const volume = document.querySelector(".volume");
const showPercentValue = document.querySelector(".show-percent-value");
const volumeBtn = document.querySelector(".volume-btn");
const slider = document.querySelector(".slider");
const rateSpeed = document.querySelector(".speed-rate");
const speedBtn = document.querySelector(".speed-btn");
const centerIcon = document.querySelector(".center-icon");
// preview
const previewImg = document.querySelector(".preview-img");
const upTime = document.querySelector(".upTime");
// screen shot
const closeModalBtn = document.querySelector(".close-btn-modal");
const modal = document.querySelector(".modal");
// dark mode
const DarkIcon = document.querySelector(".dark-icon");
const LightIcon = document.querySelector(".light-icon");
// const themeToggle = document.querySelector(".theme-toggle");
const userTheme = localStorage.getItem("theme");
const systemTheme = window.matchMedia("(prefers-color-scheme : dark").matches;
// subtitle or caption
const subtitleBtn = document.querySelector(".subtitle-btn");
// information key
const info = document.querySelector(".info");
// get title video
let videoSrc = video.src;
let srcSplitVideo = videoSrc.split("/");
let SrcIndexVideo = srcSplitVideo[srcSplitVideo.length - 1];
let splitNameVideo = SrcIndexVideo.split(".");
let indexNameVideo = splitNameVideo[splitNameVideo.length - 2];
let NameVideo = indexNameVideo.split("-");
let mainNameVideo = NameVideo.join(" ");
// videoTitleChange.textContent = mainNameVideo;

// dark mode : theme check
const themeCheck = () => {
  if (userTheme === "dark" || (!userTheme && systemTheme)) {
    document.documentElement.classList.add("dark");
    DarkIcon.classList.add("hidden");
    return;
  }
  LightIcon.classList.add("hidden");
};
// volume : mute
volumeBtn.addEventListener("click", muteHandler);
function muteHandler() {
  video.muted = !video.muted;
}
// play & pause  video
video.addEventListener("click", togglePlay);
playBtn.addEventListener("click", togglePlay);
function togglePlay() {
  video.paused || video.ended ? video.play() : video.pause();
}
video.addEventListener("play", () => {
  playPauseBtn.classList.toggle("playing");
});
video.addEventListener("pause", () => {
  playPauseBtn.classList.toggle("playing");
});

// digit round time
const addZeroToTime = new Intl.NumberFormat(undefined, {
  minimumIntegerDigits: 2,
});
// format time
function formatTime(t) {
  const hours = Math.floor(t / 3600);
  const minutes = Math.floor(t / 60) % 60;
  const seconds = Math.floor(t % 60);
  if (hours === 0) {
    return `${addZeroToTime.format(minutes)}:${addZeroToTime.format(seconds)}`;
  } else {
    return `${hours}:${addZeroToTime.format(minutes)}:${addZeroToTime.format(seconds)}`;
  }
}
// view : button full screen
fullScreen.addEventListener("click", fullScreenHandler);
function fullScreenHandler() {
  if (document.fullscreenElement == null) {
    container.requestFullscreen();
    fullScreenOpen.classList.add("hidden");
    fullScreenExit.classList.remove("hidden");
  } else {
    document.exitFullscreen();
    fullScreenOpen.classList.remove("hidden");
    fullScreenExit.classList.add("hidden");
  }
}
// view : enable full screen instead native
document.addEventListener("fullscreenchange", () => {
  container.classList.toggle("fullscreen", document.fullscreenElement);
});
// view : full picture to picture
video.addEventListener("enterpictureinpicture", () => {
  video.classList.add("mini-picture");
});
video.addEventListener("leavepictureinpicture", () => {
  video.classList.remove("mini-picture");
});
miniScreen.addEventListener("click", miniScreenHandler);
function miniScreenHandler() {
  if (video.classList.contains("mini-picture")) {
    document.exitPictureInPicture();
  } else {
    video.requestPictureInPicture();
  }
}
// show & hide buttons
var time1;
const hideButton = () => {
  if (video.paused) return;
  time1 = setTimeout(() => {
    slider.classList.add("opacity-0");
    info.classList.add("hidden");
  }, 3000);
};
hideButton();
container.addEventListener("mousemove", () => {
  slider.classList.remove("opacity-0");
  clearTimeout(time1);
  hideButton();
});
// speed time
rateSpeed.querySelectorAll("li").forEach((item) => {
  item.addEventListener("click", () => {
    video.playbackRate = item.dataset.speed;
    rateSpeed.querySelector("li.active").classList.remove("active");
    item.classList.add("active");
  });
});
speedBtn.addEventListener("click", () => {
  rateSpeed.classList.toggle("hidden");
});
let showRate;
rateSpeed.addEventListener("mouseleave", () => {
  showRate = setTimeout(() => {
    rateSpeed.classList.add("hidden");
  }, 2000);
});
rateSpeed.addEventListener("mousemove", () => {
  clearTimeout(showRate);
});
// icon center
video.addEventListener("playing", function () {
  centerIcon.style.opacity = 0;
});
video.addEventListener("pause", function () {
  centerIcon.style.opacity = 1;
});
// management video player with keyboard
document.addEventListener("keydown", (i) => {
  const tagName = document.activeElement.tagName.toLowerCase();
  if (tagName === "input") return;
  switch (i.key.toLowerCase()) {
    case " ":
      if (tagName === "button") return;
    case "k":
      togglePlay();
      break;
    case "f":
      fullScreenHandler();
      break;
    case "i":
      miniScreenHandler();
      break;
    case "m":
      muteHandler();
      break;
    case "c":
      captionHandler();
      break;
    case "arrowup":
      volumeUp();
      break;
    case "arrowdown":
      volumeDown();
      break;
    case "arrowright":
      skipForward();
      break;
    case "arrowleft":
      skipBackward();
      break;
  }
});
